# BotswanaEMR OpenMRS Module

## Overview

Configuration module for the BostwanaEMR distribution

This module provides the following configuration meta-data:

* [ ] Look and feel customizations


## Setting up a development environment

Download OpenMRS Reference Application 2.12.2 modules from: https://sourceforge.net/projects/openmrs/files/releases/OpenMRS_Reference_Application_2.12.2/referenceapplication-addons-2.12.2.zip/download
Unpack this zip file and copy all modules into the {OPENMRS_HOME}/modules folder
Download the OpenMRS Platform 2.5.0 from source
Download the following module and upgrade the existing module:
htmlformentry-4.3.0 from https://addons.openmrs.org/show/org.openmrs.module.htmlformentry
Download, build and install this module to integrate with the above mentioned packages
Run the integrated package as a normal OpenMRS reference Application using https://wiki.openmrs.org/display/docs/Reference+Application+2.11.0 as a sample guide